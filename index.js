require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const loginRouter = require('./routes/login');
const orderRouter = require('./routes/order');
// url for docker
// const { url } = require('./config/db.config');

const OrderModel = require('./models/order.model');

const port = process.env.PORT || 4242;
const app = express();

app.use(cors({
  origin: "*"
}));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// url for local db
// const url = 'mongodb://localhost:27017/pizza';
// url for cloud db
const url = `mongodb+srv://JohnPa:${process.env.MONGO_PWD}@cluster0.zypj0.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`

mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Successfully connect to MongoDB.');
  })
  .catch((err) => {
    console.error('Connection error', err);
    process.exit();
  });

app.get('/hello', (req, res) => {
  OrderModel.find({}, (err, data) => {
    if (err) res.send(err);
    else if (!data.length) {
      const orders = [
        {
          username: 'John',
          quantity: 2,
          date: new Date(),
        },
        {
          username: 'John',
          quantity: 1,
          date: new Date(),
        },
        {
          username: 'John',
          quantity: 3,
          date: new Date(),
        },
        {
          username: 'Doe',
          quantity: 1,
          date: new Date(),
        },
        {
          username: 'Doe',
          quantity: 1,
          date: new Date(),
        },
        {
          username: 'Doe',
          quantity: 3,
          date: new Date(),
        },
        {
          username: 'Ginette',
          quantity: 2,
          date: new Date(),
        },
        {
          username: 'Bob',
          quantity: 3,
          date: new Date(),
        },
        {
          username: 'Ginette',
          quantity: 6,
          date: new Date(),
        },
        {
          username: 'Bob',
          quantity: 10,
          date: new Date(),
        },
      ];
      OrderModel.create(orders, () => {
        if (err) res.send(err);
        else res.send('hello');
      });
    } else res.send('hello');
  });
});

app.use(loginRouter);
app.use(orderRouter);

app.use((error, req, res, next) => {
  if (error.type === 'username') { res.status(403).send({ error: 'Login length between 4 and 16', type: 'username' }); }
  res.status(500).send('Error 500 buddy');
  next();
});

app.listen(port, () => {
  console.log(`Running on port ${port}`);
});
