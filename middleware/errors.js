const errorResponse = (error, req, res, next) => {
  if (error.type === 'username') { res.status(403).send({ error: 'Login length between 4 and 16', type: 'username' }); }
  res.status(500).send('Error 500 buddy');
};

module.exports = {
  errorResponse,
};
