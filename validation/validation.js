function usernameValidation(username) {
  return username.length > 16 || username.length < 4;
}

module.exports = {
  usernameValidation,
};
