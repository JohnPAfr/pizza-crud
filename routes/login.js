require('dotenv').config();
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const UserModel = require('../models/user.model');
const { usernameValidation } = require('../validation/validation');

const loginRouter = express.Router();

loginRouter.post('/login', async (req, res, next) => {
  const user = await UserModel.findOne({ username: req.body.username });
  const { username, password } = req.body;

  if (user) {
    try {
      if (await bcrypt.compare(password, user.password)) {
        const token = jwt.sign(user.toJSON(), process.env.SECRET_TOKEN);
        res.json({ jwt: token, user });
      } else { res.status(400).send({ error: 'Password is incorrect', type: 'password' }); }
    } catch (error) {
      next({ error: 'Jwt error', type: 'global' });
    }
  } else {
    try {
      if (usernameValidation(username)) next({ error: 'Login length between 4 and 16', type: 'username' });
      else {
        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = UserModel();
        newUser.username = req.body.username;
        newUser.password = hashedPassword;

        newUser.save((err, data) => {
          if (err) {
            res.status(400).send({ error: 'Error during user creation', type: 'global' });
          } else {
            const token = jwt.sign(newUser.toJSON(), process.env.SECRET_TOKEN);
            res.status(201).json({ user: data, jwt: token });
          }
        });
      }
    } catch {
      next({ error: 'Duh ?' });
    }
  }
});

module.exports = loginRouter;
